import * as React from "react";
import {Switch, Route, HashRouter as HRouter} from "react-router-dom";
import {Home} from "./tabs/homeTab/Home";
import {Feature} from "./tabs/featureTab/Feature";
import {Calculator} from "./tabs/calculator/Calculator";

interface Istate {
    colour: string,
    colourSet: string[]
}

export class Router extends React.Component<any, Istate> {

    public state: Istate = {
        colour: "black",
        colourSet: ["black", "blue", "red", "yellow", "green", "orange"]
    };

    public render() {
        return (
            <HRouter>
                <main>
                    {/*<button id={"sdf"} onClick={(e: any) => this.changecolor(e)}>change color</button>*/}
                    <div className="container-fluid router-mainbody">
                        <Switch>
                            <Route exact
                                   path={'/'}
                                   component={() =>
                                       <Home color={this.state.colour} className="home" exact/>
                                   }
                            />
                            <Route exact
                                   path={'/Feature'}
                                   component={() =>
                                       <Feature exact/>
                                   }
                            />
                            <Route exact
                                   path={'/Calculator'}
                                   component={() =>
                                       <Calculator/>
                                   }
                            />
                        </Switch>
                    </div>
                </main>
            </HRouter>
        );
    }

    private changecolor(e: any) {
        this.setState({colour: this.state.colour == 'black' ? 'red' : this.state.colour == 'red' ? 'orange' : 'black'});
        console.log(e.target.id);
    }
}
