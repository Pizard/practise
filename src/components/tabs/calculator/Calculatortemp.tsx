import * as React from "react";

export class Calculatortemp extends React.Component<any, IState> {

    constructor() {
        super();
        this.state = {
            variables: [],
            index: 0,
            operation: "",
            result: ""
        };
    }

    render() {
        return (
            <div>
                <div>display - {this.state.variables[this.state.index-1]} {this.state.operation} {this.state.variables[this.state.index]}</div>

                <div>result - {this.state.result}</div>
                <div>
                    <button onClick={() => this.capture('1')}>1</button>
                    <button onClick={() => this.capture('2')}>2</button>
                    <button onClick={() => this.capture('3')}>3</button>
                    <button onClick={() => this.func('add')}>+</button>
                </div>
                <div>

                    <button onClick={() => this.capture('4')}>4</button>
                    <button onClick={() => this.capture('5')}>5</button>
                    <button onClick={() => this.capture('6')}>6</button>
                    <button onClick={() => this.func('sub')}>-</button>
                </div>
                <div>

                    <button onClick={() => this.capture('7')}>7</button>
                    <button onClick={() => this.capture('8')}>8</button>
                    <button onClick={() => this.capture('9')}>9</button>
                    <button onClick={() => this.func('mul')}>x</button>
                </div>
                <div>

                    <button onClick={() => this.capture('0')}>0</button>
                    <button onClick={() => this.func('eql')}>=</button>
                    <button onClick={() => this.func('div')}>/</button>
                </div>
            </div>
        )
    }

    private capture(value: string) {
        if(this.state.variables[this.state.index] != null) {
            let temp = this.state.variables;
            temp[this.state.index] = this.state.variables[this.state.index].concat(value);
            this.setState({variables: temp});
        }else{
            let temp = this.state.variables;
            temp[this.state.index] = value;
            this.setState({variables: temp});
        }

    }

    private func(work: string) {
        if(this.state.index == 0){
            this.setState({operation: work, index: this.state.index + 1})
        }else{
            this.doOperation(this.state.operation,work);
        }
    }

    private doOperation(work: string, operation: string) {
        if(this.state.variables.length>1){
        switch (work){
            case "add": {
                    let value: number = parseInt(this.state.variables[0]) + parseInt(this.state.variables[1]);
                    let temp = [];
                    temp[0] = value.toString();
                    this.setState({operation: operation, result: value.toString(), index: 1, variables: temp});

                break;
            }
            case "sub": {
                    let value: number = parseInt(this.state.variables[0]) - parseInt(this.state.variables[1]);
                    let temp = [];
                    temp[0] = value.toString();
                    this.setState({operation: operation, result: value.toString(), index: 1, variables: temp});

                break;
            }
            case "mul": {
                    let value: number = parseInt(this.state.variables[0]) * parseInt(this.state.variables[1]);
                    let temp = [];
                    temp[0] = value.toString();
                    this.setState({operation: operation, result: value.toString(), index: 1, variables: temp});

                break;
            }
            case "div": {
                    if(parseInt(this.state.variables[1]) !== 0) {
                        let value: number = parseInt(this.state.variables[0]) / parseInt(this.state.variables[1]);
                        let temp = [];
                        temp[0] = value.toString();
                        this.setState({operation: operation, result: value.toString(), index: 1, variables: temp});
                    }

                break;
            }
            default: {
                let value: number = work == 'add' ? parseInt(this.state.variables[0]) + parseInt(this.state.variables[1]) :
                                    work == 'sub' ? parseInt(this.state.variables[0]) - parseInt(this.state.variables[1]) :
                                    work == 'mul' ? parseInt(this.state.variables[0]) * parseInt(this.state.variables[1]) :
                                    parseInt(this.state.variables[0]) / parseInt(this.state.variables[1]);
                let temp = [];
                temp[0] = value.toString();
                this.setState({operation: operation, result: value.toString(), index: 1, variables: temp});
                break;
            }
        }
    }else{this.setState({operation: operation});}
    }

}

interface IState {
    variables: string[],
    index: number,
    operation: string,
    result: string
}