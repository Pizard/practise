import * as React from "react";

export class Calculator extends React.Component<any, IState> {

    constructor() {
        super();
        this.state = {
            num: 1,
            num1: 0,
            num2: 0,
            index: '',
            result: 0
        };
    }

    render() {
        console.log(this.state);
        return (
            <div>
                <div>Exp ->  {this.state.num1 != 0 ? this.state.num1:null} {this.state.index != 'eql' ? this.state.index:null} {this.state.num2!= 0 ? this.state.num2 : null}</div>
                <div>Result -> {this.state.num1 != 0 ? this.state.num1:null}</div>
                <div>
                    <button onClick={() => this.capture(1)}>1</button>
                    <button onClick={() => this.capture(2)}>2</button>
                    <button onClick={() => this.capture(3)}>3</button>
                    <button onClick={() => this.func('add')}>+</button>
                </div>
                <div>
                    <button onClick={() => this.capture(4)}>4</button>
                    <button onClick={() => this.capture(5)}>5</button>
                    <button onClick={() => this.capture(6)}>6</button>
                    <button onClick={() => this.func('sub')}>-</button>
                </div>
                <div>
                    <button onClick={() => this.capture(7)}>7</button>
                    <button onClick={() => this.capture(8)}>8</button>
                    <button onClick={() => this.capture(9)}>9</button>
                    <button onClick={() => this.func('mul')}>x</button>
                </div>
                <div>
                    <button onClick={() => this.capture(0)}>0</button>
                    <button onClick={() => this.func('eql')}>=</button>
                    <button onClick={() => this.func('div')}>/</button>
                    <button onClick={() => this.func('clr')}>C</button>
                </div>
            </div>
        )
    }

    private capture(value: number) {
        if (this.state.num == 1) {
            this.setState({num1: (this.state.num1 * 10) + value});
        }
        else {
            this.setState({num2: (this.state.num2 * 10) + value});
        }
    }

    private func(work: string) {
        if (work != 'clr') {
            if (this.state.num == 1) {
                this.setState({index: work, num: 2});
            }
            else {
                if (this.state.num2 != 0) {
                    switch (this.state.index) {
                        case 'add': {
                            let result: number = this.state.num1 + this.state.num2;
                            this.setState({num1: result, num2: 0, index: work});
                            break;
                        }
                        case 'sub': {
                            let result: number = this.state.num1 - this.state.num2;
                            this.setState({num1: result, num2: 0, index: work});
                            break;
                        }
                        case 'mul': {
                            let result: number = this.state.num1 * this.state.num2;
                            this.setState({num1: result, num2: 0, index: work});
                            break;
                        }
                        case 'div': {
                            if(this.state.num2 == 0){

                            }
                            let result: number = this.state.num1 / this.state.num2;
                            this.setState({num1: result, num2: 0, index: work});
                            break;
                        }
                        default: {
                            let result: number = work == 'add' ? this.state.num1 + this.state.num2 :
                                work == 'sub' ? this.state.num1 - this.state.num2 :
                                    work == 'mul' ? this.state.num1 * this.state.num2 :
                                        this.state.num1 / this.state.num2;
                            this.setState({num1: result, num2: 0, index: work});
                            break;
                        }
                    }
                }
                else {
                    this.setState({index: work});
                }

            }
        }
        else {
            this.setState({num: 1, index: '', num2: 0, num1: 0, result: 0});
        }
    }

}

interface IState {
    num1: number,
    num2: number,
    index: string,
    result: number,
    num: number
}